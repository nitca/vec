def make_email(login, hosts):
    emails = []
    for host in hosts:
        tmp_login = login
        tmp_login += '@'
        tmp_login += host
        emails.append(tmp_login)
    
    return emails
